package User;

import java.util.*;

public class Person extends Entity {
	
	private static String firstName;
	private static String surname;
	private static String pesel;
	private static Date dateOfBirth;
	
	public static String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public static String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public static String getPesel() {
		return pesel;
	}
	
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	
	public static Date getDate(){
		return dateOfBirth;
	}
	
	public void setDate(Date DOB){
		this.dateOfBirth = DOB;
	}

}
