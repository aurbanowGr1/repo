package repozytorium;

import java.util.*;

public interface Repository <TEntity> {
	
		public void add(TEntity entity);
		public void modify(TEntity entity);
		public void delete(TEntity entity);
		public void count();

}
