package repozytorium;

import java.sql.Connection;

import unitOfWork.*;
import User.*;

public class RepositoryCatalog implements RepoCatalog {

	private Connection connection;
	private UnitOfWork Unit;

	public RepositoryCatalog(Connection connection, UnitOfWork uow){
		super();
		this.connection = connection;
		this.Unit = uow;
	}
	
	
	@Override
	public UserRepo getUsers() {
		return new UserRepository(connection, new CreateUser(), Unit);
	}

	@Override
	public Repository<UserRoles> getRoles() {
		return null;
	}

	@Override
	public void commit() {
		Unit.commit();
	}


	@Override
	public Repository<Person> getPersons() {
	
		return null;
	}

}
